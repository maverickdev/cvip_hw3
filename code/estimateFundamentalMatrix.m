function estimateFundamentalMatrix(I1,I2,groundTruth)
    %%
    %% load images and match files for the first example
    %%
    
    matches = groundTruth;
    displayMatchPointPairs(I1,I2,matches);
    title('Feature Points')
    %% Unnormalized Algorithm
    F = fit_fundamental(matches);
    displayEpipolarLinesAndPoints(F,matches,I2,'Unnomralized Algorithm');
    averageResidual = mean(fundamentalMatrixError(matches,F));
    fprintf('\nUnnormalized Average Non Linear Squared Distance: %.3f ',averageResidual);

    %% Normalized Algorithm - Standardisation
    F = fit_fundamental_normalized(matches);
    displayEpipolarLinesAndPoints(F,matches,I2,'Nomralized Algorithm');
    averageResidual = mean(fundamentalMatrixError(matches,F));
    fprintf('\nNormalized Average Non Linear Squared Distance: %.3f ',averageResidual);
    
    %% RANSAC
    matches = getPointMatches(I1,I2);
    % this is a N x 4 file where the first two numbers of each row
    % are coordinates of corners in the first image and the last two
    % are coordinates of corresponding corners in the second image: 
    % matches(i,1:2) is a point in the first image
    % matches(i,3:4) is a corresponding point in the second image
    
    displayMatchPointPairs(I1,I2,matches);
    title('Feature Points - Harris');
    
    [F,matchIdxs,averageResidual] = fit_fundamental_ransac(matches);
    fprintf('\nRANSAC Average Non Linear Squared Distance: %.3f ',averageResidual);
    m = matches(matchIdxs,:);
    matches = m;
    displayEpipolarLinesAndPoints(F,matches,I2,'RANSAC');
end

function displayMatchPointPairs(I1,I2,matches)
    %%
    %% display two images side-by-side with matches
    %% this code is to help you visualize the matches, you don't need
    %% to use it to produce the results for the assignment
    %%
    figure;
    imshow([I1 I2]); hold on;
    plot(matches(:,1), matches(:,2), '+r');
    plot(matches(:,3)+size(I1,2), matches(:,4), '+r');
    line([matches(:,1) matches(:,3) + size(I1,2)]', matches(:,[2 4])', 'Color', 'r');
end

function displayEpipolarLinesAndPoints(F,matches,I2,method)
    %%
    %% display second image with epipolar lines reprojected
    %% from the first image
    %%
    N = size(matches,1);
    L = (F * [matches(:,1:2) ones(N,1)]')'; % transform points from 
    % the first image to get epipolar lines in the second image

    % find points on epipolar lines L closest to matches(:,3:4)
    L = L ./ repmat(sqrt(L(:,1).^2 + L(:,2).^2), 1, 3); % rescale the line
    pt_line_dist = sum(L .* [matches(:,3:4) ones(N,1)],2);
    closest_pt = matches(:,3:4) - L(:,1:2) .* repmat(pt_line_dist, 1, 2);

    % find endpoints of segment on epipolar line (for display purposes)
    pt1 = closest_pt - [L(:,2) -L(:,1)] * 10; % offset from the closest point is 10 pixels
    pt2 = closest_pt + [L(:,2) -L(:,1)] * 10;

    % display points and segments of corresponding epipolar lines
    figure;
    imshow(I2); hold on;
    title(strcat('Features and Epipolar Lines-',method));
    plot(matches(:,3), matches(:,4), '+r');
    line([matches(:,3) closest_pt(:,1)]', [matches(:,4) closest_pt(:,2)]', 'Color', 'r');
    line([pt1(:,1) pt2(:,1)]', [pt1(:,2) pt2(:,2)]', 'Color', 'g');
end