function [fundamentalMatrix,maxInliners,averageResidual] = fit_fundamental_ransac(points)
    maxIterations=10000;
    pointsPairsRequiredForModel = 8;
    iterations = 0;
    fundamentalMatrix = zeros(3,3);
    maxError = 300;
    maxInlinerCount = 0;
    maxInliners = [];
    averageResidual = 0;
    while iterations < maxIterations
        pointPairCount = size(points,1);
        chosenIndices = randperm(pointPairCount,pointsPairsRequiredForModel);
        points_F = points(chosenIndices,:);
        maybemodel = fit_fundamental_normalized(points_F);
        error = fundamentalMatrixError(points,maybemodel);
        inliners = find(error < maxError);
        if size(inliners,2) > maxInlinerCount
            fundamentalMatrix = maybemodel;
            maxInlinerCount = size(inliners,2);
            maxInliners = inliners;
            averageResidual = mean(error(inliners));
            disp(maxInlinerCount)
        end
        iterations = iterations+1;
    end
    disp(1);
end