function fundamentalMatrix = fit_fundamental(points)
    A = zeros(size(points,1),9);
    for i = 1: size(points,1)
        x = points(i,1);
        xd = points(i,3);
        y = points(i,2);
        yd = points(i,4);
        xxd = x*xd;
        yxd = y*xd;
        yyd = y*yd;
        xyd = x*yd;
        A(i,:)=[xxd,yxd,xd,xyd,yyd,yd,x,y,1];
    end
    [~,~,V] = svd(A);
    F = V(:,end);
    F = reshape(F,[3,3])';
    [FU,FD,FV] = svd(F);
    FD(end) = 0;
    fundamentalMatrix = FU * FD * FV';
end
