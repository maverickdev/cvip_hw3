imageL = imread('../data/part1/uttower/left.jpg');
imageR = imread('../data/part1/uttower/right.jpg');

stitchImages(imageL,imageR);


I1 = imread('../data/part2/house1.jpg');
I2 = imread('../data/part2/house2.jpg');
groundTruth_house = load('../data/part2/house_matches.txt'); 
estimateFundamentalMatrix(I1,I2,groundTruth_house);

I1 = imread('../data/part2/library1.jpg');
I2 = imread('../data/part2/library2.jpg');
groundTruth_library = load('../data/part2/library_matches.txt'); 
estimateFundamentalMatrix(I1,I2,groundTruth_library);

P1 = load('../data/part2/house1_camera.txt');
P2 = load('../data/part2/house2_camera.txt');
extractTriangulation(P1,P2,groundTruth_house);

P1 = load('../data/part2/library1_camera.txt');
P2 = load('../data/part2/library2_camera.txt');
extractTriangulation(P1,P2,groundTruth_library);