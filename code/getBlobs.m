%Code from HW2
%Author Kaushik Panneerselvam
function blobs = getBlobs(scaleSpace,initialScale,scalingFactor,threshold)
    [h,w,scaleSpaceSize] = size(scaleSpace); 
    [maxImage,idx] = max(scaleSpace,[],3);
    ordFiltVals = ordfilt2(maxImage, 3*3, ones(3));
    nonMaxSuppressed = maxImage .* (ordFiltVals == maxImage);
    nonMaxSuppressed(nonMaxSuppressed<threshold) = 0;
    
    scaleRadius = zeros(scaleSpaceSize,1);
    scale = initialScale;
    for i=1:scaleSpaceSize
        scaleRadius(i) = scale * sqrt(2);
        scale = scale*scalingFactor;
    end
    
    blobs = (nonMaxSuppressed >0) .* scaleRadius(idx);
    
end