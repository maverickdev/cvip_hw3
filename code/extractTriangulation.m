function extractTriangulation(P1,P2,groundTruth)
    [~,~,V1] = svd(P1);
    center1 = V1(:,end);
    center1 = reshape(center1,[4,1])'/center1(4);
    [~,~,V2] = svd(P2);
    center2 = V2(:,end);
    center2 = reshape(center2,[4,1])'/center2(4);
    N = size(groundTruth,1);
    threeDPoints = zeros(N,4);
    p1=P1(1,:);
    p2=P1(2,:);
    p3=P1(3,:);
    p1d=P2(1,:);
    p2d=P2(2,:);
    p3d=P2(3,:);
    for i = 1:N
        pointpair = groundTruth(i,:);
        x = pointpair(1);
        y = pointpair(2);
        xd = pointpair(3);
        yd = pointpair(4);
        A = [y*p3 - p2; p1 - x*p3; yd*p3d - p2d; p1d - xd*p3d];
        [~,~,V] = svd(A);
        X = V(:,end);
        X = X/X(end);
        threeDPoints(i,:) = X;
    end
    figure;
    scatter3(threeDPoints(:,1),threeDPoints(:,2),threeDPoints(:,3)); hold on;
    centers = [center1(1:3);center2(1:3)];
    scatter3(centers(:,1),centers(:,2),centers(:,3), '+r');
    axis equal;
    
end