%Code from HW2
%Author Kaushik Panneerselvam
function [r,c,rads] = blobDetector(im,scaleSpaceSize,initialScale,scalingFactor,threshold)
    scaleSpace = getScaleSpace(im,scaleSpaceSize,initialScale,scalingFactor);
    blobs = getBlobs(scaleSpace,initialScale,scalingFactor,threshold);
    [r,c,rads]=find(blobs);
end