function [im,averageResidual] = stitchImages(imageL,imageR)
    imL = rgb2gray(imageL);
    imL = im2double(imL);

    imR = rgb2gray(imageR);
    imR = im2double(imR);
    
    initialScale = 8;
    scalingFactor=2;
    scaleSpaceSize = 4;
    threshold_blobs = 0.1;

    [rb_L,cb_L,rads_L] = blobDetector(imL,scaleSpaceSize,initialScale,scalingFactor,threshold_blobs);
    circles_L = zeros(size(rads_L,1),3);
    circles_L(:,1) = cb_L;
    circles_L(:,2) = rb_L;
    circles_L(:,3) = rads_L;

    [rb_R,cb_R,rads_R] = blobDetector(imR,scaleSpaceSize,initialScale,scalingFactor,threshold_blobs);
    circles_R = zeros(size(rads_R,1),3);
    circles_R(:,1) = cb_R;
    circles_R(:,2) = rb_R;
    circles_R(:,3) = rads_R;

    sift_arr_L = find_sift(imL,circles_L,1.5);
    sift_arr_R = find_sift(imR,circles_R,1.5);

    distanceMatrix = dist2(sift_arr_L,sift_arr_R);

    threshold = 0.1;
    [leftIdx,rightIdx,~] = find(distanceMatrix<threshold);
    noOfFeaturesConsidered = size(leftIdx,1);

    point_L = zeros(2,noOfFeaturesConsidered);
    point_R = zeros(2,noOfFeaturesConsidered);
    for i = 1: noOfFeaturesConsidered
        point_L(:,i) = [cb_L(leftIdx(i)),rb_L(leftIdx(i))];
        point_R(:,i) = [cb_R(rightIdx(i)),rb_R(rightIdx(i))];
    end
    match_plot(imageL,imageR,point_L,point_R);

    [homography, inliers,averageResidual,inlierRatio] = ransac(point_L,point_R);
    match_plot(imageL,imageR,point_L',point_R');
    title('Putative Matches');
    figure;
    match_plot(imageL,imageR,point_L(:,inliers)',point_R(:,inliers)')
    title('RANSAC Inliners');

    tform = maketform('projective', homography');
    [timage_L, x, y] = imtransform(imageL, tform);
    padding=ceil([-y(1),-x(1)]);
    imageRp = padarray(imageR, padding, 'pre');
    paddingL = size(imageRp) - size(timage_L);
    imageLp = padarray(timage_L, paddingL, 'post');
    im = max(imageLp,imageRp);
    figure;
    imshow(im);
    title('Stitched Image');
    fprintf('Inlier Count: %i ',size(inliers,2));
    fprintf('Inlier Ratio: %.3f ',inlierRatio);
    fprintf('Average Residual: %.3f ',averageResidual);
end