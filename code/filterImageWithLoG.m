%Code from HW2
%Author Kaushik Panneerselvam
function [filterResponse] = filterImageWithLoG(im,size,scale)
    filter = fspecial('log', size , scale);
    filter = (scale*scale) * filter;
    filterResponse = imfilter(im,filter,'replicate');
end