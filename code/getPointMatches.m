function matches = getPointMatches(I1,I2)
    im1 = rgb2gray(I1);
    im1 = im2double(im1);
    
    im2 = rgb2gray(I2);
    im2 = im2double(im2);
    
    [r1,c1] = size(im1);
    [r2,c2] = size(im2);
    
    sigma = 1;
    threshold = 0.02;
    radius = 3;

    [~,r_L,c_L] = harris(im1,sigma,threshold,radius);
    featurePoint_L = zeros(size(c_L,1),2);
    featurePoint_L(:,1) = c_L;
    featurePoint_L(:,2) = r_L;

    [~,r_R,c_R] = harris(im2,sigma,threshold,radius);
    featurePoint_R = zeros(size(c_R,1),2);
    featurePoint_R(:,1) = c_R;
    featurePoint_R(:,2) = r_R;
    
    localNeighborHoodSize = 27;
    halfSize = floor(localNeighborHoodSize/2);
    featurePoint_L = featurePoint_L((featurePoint_L(:,1) > halfSize & featurePoint_L(:,2) > halfSize & featurePoint_L(:,1) < c1-halfSize & featurePoint_L(:,2) < r1-halfSize),:);
    featurePoint_R = featurePoint_R((featurePoint_R(:,1) > halfSize & featurePoint_R(:,2) > halfSize & featurePoint_R(:,1) < c2-halfSize & featurePoint_R(:,2) < r2-halfSize),:);
    
    featureCount_L = size(featurePoint_L,1);
    features_L = zeros(localNeighborHoodSize*localNeighborHoodSize,featureCount_L);
    for i = 1: featureCount_L
        feature = im1(featurePoint_L(i,2) - halfSize : featurePoint_L(i,2) + halfSize,featurePoint_L(i,1) - halfSize : featurePoint_L(i,1) + halfSize);
        features_L(:,i) = reshape(feature,[localNeighborHoodSize*localNeighborHoodSize,1]);
    end
    
    
    featureCount_R = size(featurePoint_R,1);
    features_R = zeros(localNeighborHoodSize*localNeighborHoodSize,featureCount_R);
    for i = 1: featureCount_R
        feature = im2(featurePoint_R(i,2) - halfSize : featurePoint_R(i,2) + halfSize,featurePoint_R(i,1) - halfSize : featurePoint_R(i,1) + halfSize);
        features_R(:,i) = reshape(feature,[localNeighborHoodSize*localNeighborHoodSize,1]);
    end
    
    distanceMatrix = dist2(features_L',features_R');

    LFeatureCount = size(distanceMatrix,1);
    RFeatureCount = size(distanceMatrix,2);
    rightIdx = [];
    leftIdx = [];
    if(LFeatureCount < RFeatureCount)
        [~,rightIdx] = min(distanceMatrix, [] ,2);
        noOfFeaturesConsidered = LFeatureCount;
        leftIdx = 1:noOfFeaturesConsidered;
    else
        [~,leftIdx] = min(distanceMatrix,[],1);
        noOfFeaturesConsidered = RFeatureCount;
        rightIdx = 1:noOfFeaturesConsidered;
    end
    matches = zeros(noOfFeaturesConsidered,4);
    for i = 1: noOfFeaturesConsidered
        matches(i,:) = [featurePoint_L(leftIdx(i),1),featurePoint_L(leftIdx(i),2),featurePoint_R(rightIdx(i),1),featurePoint_R(rightIdx(i),2)];
    end
end