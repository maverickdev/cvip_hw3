function error =  fundamentalMatrixError(points,fundamentalMatrix)
    X1 = [points(:,1:2) ones(size(points,1),1)];
    X2 = [points(:,3:4) ones(size(points,1),1)];
    pointCount = size(points,1);
    error = zeros(size(pointCount));
    for i = 1:pointCount
        error(i) = sqrt(sum((X2(i,:)' - (fundamentalMatrix * X1(i,:)')).^2) + sum(((X2(i,:)*fundamentalMatrix)' - X1(i,:)').^2));
    end
end