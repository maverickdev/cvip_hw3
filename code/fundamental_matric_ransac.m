%%
%% load images and match files for the first example
%%

I1 = imread('house1.jpg');
I2 = imread('house2.jpg');
%matches = load('house_matches.txt');

im1 = rgb2gray(I1);
im1 = im2double(im1);

im2 = rgb2gray(I2);
im2 = im2double(im2);

initialScale = 8;
scalingFactor=2;
scaleSpaceSize = 4;
threshold_blobs = 0.1;

[rb_L,cb_L,rads_L] = blobDetector(im1,scaleSpaceSize,initialScale,scalingFactor,threshold_blobs);
circles_L = zeros(size(rads_L,1),3);
circles_L(:,1) = cb_L;
circles_L(:,2) = rb_L;
circles_L(:,3) = rads_L;

[rb_R,cb_R,rads_R] = blobDetector(im2,scaleSpaceSize,initialScale,scalingFactor,threshold_blobs);
circles_R = zeros(size(rads_R,1),3);
circles_R(:,1) = cb_R;
circles_R(:,2) = rb_R;
circles_R(:,3) = rads_R;

sift_arr_L = find_sift(im1,circles_L,1.5);
sift_arr_R = find_sift(im2,circles_R,1.5);

distanceMatrix = dist2(sift_arr_L,sift_arr_R);

threshold = 0.1;
[leftIdx,rightIdx,~] = find(distanceMatrix<threshold);
noOfFeaturesConsidered = size(leftIdx,1);

% point_L = zeros(noOfFeaturesConsidered,4);
% point_R = zeros(noOfFeaturesConsidered,4);
matches = zeros(noOfFeaturesConsidered,4);
for i = 1: noOfFeaturesConsidered
    matches(i,:) = [cb_L(leftIdx(i)),rb_L(leftIdx(i)),cb_R(rightIdx(i)),rb_R(rightIdx(i))];
end
%match_plot(imageL,imageR,point_L,point_R);

%[homography, inliers,averageResidual] = ransac_fundamental_matrix(point_L,point_R);

% this is a N x 4 file where the first two numbers of each row
% are coordinates of corners in the first image and the last two
% are coordinates of corresponding corners in the second image: 
% matches(i,1:2) is a point in the first image
% matches(i,3:4) is a corresponding point in the second image

N = size(matches,1);

%%
%% display two images side-by-side with matches
%% this code is to help you visualize the matches, you don't need
%% to use it to produce the results for the assignment
%%
imshow([I1 I2]); hold on;
plot(matches(:,1), matches(:,2), '+r');
plot(matches(:,3)+size(I1,2), matches(:,4), '+r');
line([matches(:,1) matches(:,3) + size(I1,2)]', matches(:,[2 4])', 'Color', 'r');
%pause;

%%
%% display second image with epipolar lines reprojected 
%% from the first image
%%

% first, fit fundamental matrix to the matches
% F = fit_fundamental_normalized(matches); % this is a function that you should write
F = fit_fundamental_ransac(matches); % this is a function that you should write
L = (F * [matches(:,1:2) ones(N,1)]')'; % transform points from 
% the first image to get epipolar lines in the second image

% find points on epipolar lines L closest to matches(:,3:4)
L = L ./ repmat(sqrt(L(:,1).^2 + L(:,2).^2), 1, 3); % rescale the line
pt_line_dist = sum(L .* [matches(:,3:4) ones(N,1)],2);
closest_pt = matches(:,3:4) - L(:,1:2) .* repmat(pt_line_dist, 1, 2);

% find endpoints of segment on epipolar line (for display purposes)
pt1 = closest_pt - [L(:,2) -L(:,1)] * 10; % offset from the closest point is 10 pixels
pt2 = closest_pt + [L(:,2) -L(:,1)] * 10;

% display points and segments of corresponding epipolar lines
clf;
imshow(I2); hold on;
plot(matches(:,3), matches(:,4), '+r');
line([matches(:,3) closest_pt(:,1)]', [matches(:,4) closest_pt(:,2)]', 'Color', 'r');
line([pt1(:,1) pt2(:,1)]', [pt1(:,2) pt2(:,2)]', 'Color', 'g');

function fundamentalMatrix = fit_fundamental_ransac(points)
    maxIterations=10000;
    pointsPairsRequiredForModel = 8;
    iterations = 0;
    fundamentalMatrix = zeros(3,3);
    maxError = 10;
    maxInlinerCount = 0;
    maxInliners = [];
    averageResidual = 0;
    while iterations < maxIterations
        pointPairCount = size(points,2);
        chosenIndices = randperm(pointPairCount,pointsPairsRequiredForModel);
        points_F = points(chosenIndices,:);
        maybemodel = fit_fundamental_normalized(points_F);
        error = fundamentalMatrixError(points,maybemodel);
        inliners = find(error < maxError);
        if size(inliners,2) > maxInlinerCount
            fundamentalMatrix = maybemodel;
            maxInlinerCount = size(inliners,2);
            maxInliners = inliners;
            averageResidual = mean(error(inliners));
        end
        iterations = iterations+1;
    end
end

function error =  fundamentalMatrixError(points,fundamentalMatrix)
    X1 = [points(:,1:2) ones(size(points,1))];
    X2 = [points(:,3:4) ones(size(points,1))];
    error = (X2'*fundamentalMatrix * X1).^2;
end