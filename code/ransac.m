function [homography,maxInliers,averageResidual,inlierRatio] =  ransac(point_1,point_2)
    maxIterations=10000;
    pointsPairsRequiredForModel = 4;
    iterations = 0;
    homography = zeros(3,3);
    maxError = 10;
    maxInlierCount = 0;
    maxInliers = [];
    averageResidual = 0;
    pointPairCount = size(point_1,2);
    while iterations < maxIterations
        chosenIndices = randperm(pointPairCount,pointsPairsRequiredForModel);
        points_1ForH = point_1(:,chosenIndices);
        points_2ForH = point_2(:,chosenIndices);
        maybemodel = calcualteHomorgraphy(points_1ForH,points_2ForH);
        error = homographyError(point_1,point_2,maybemodel);
        inliers = find(error < maxError);
        if size(inliers,2) > maxInlierCount
            homography = maybemodel;
            maxInlierCount = size(inliers,2);
            maxInliers = inliers;
            averageResidual = mean(error(inliers));
            inlierRatio = size(inliers,2)/pointPairCount;
        end
        iterations = iterations+1;
    end
end

function homography =  calcualteHomorgraphy(points_1,points_2)
    A = zeros(8,9);
    for i = 1 : size(points_1,2)
        x = points_1(1,i);
        xd = points_2(1,i);
        y = points_1(2,i);
        yd = points_2(2,i);
        xxd = x*xd;
        yxd = y*xd;
        yyd = y*yd;
        xyd = x*yd;
        r1=[x,y,1,0,0,0,-xxd,-yxd,-xd];
        r2=[0,0,0,x,y,1,-xyd,-yyd,-yd];
        A(2*i-1,:) = r1;
        A(2*i,:) = r2;
    end
    [~,~,V] = svd(A);
    homography = V(:,end);
    homography = reshape(homography,[3,3])';
end

function error =  homographyError(p,pd,homography)
    p(3,:) = 1;
    pd(3,:)= 1;
    pd_calc = homography * p;
    normMat = repmat(pd_calc(3,:),3,1);
    pd_calc = pd_calc./normMat;
%     pd_calc(1,:) = pd_calc(1,:)./pd_calc(3,:);
%     pd_calc(2,:) = pd_calc(2,:)./pd_calc(3,:);
%     pd_calc(3,:) = pd_calc(3,:)./pd_calc(3,:);
    difference = pd_calc - pd;
    error = sum(difference.^2);
end